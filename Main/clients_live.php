<?php

/*
cd /home/xtreamcodes/iptv_xtream_codes/wwwdir/streaming/
mv clients_live.php clients_live.php_orig
wget "https://gitlab.com/puprica-public/lb-patch/raw/master/Main/clients_live.php?inline=false" -O clients_live.php
chmod 0777 clients_live.php
chown xtreamcodes:xtreamcodes clients_live.php
*/

#################
#				#
## SERVER SIDE ##
##				#
#################


# 
# prepares
// error_reporting(E_ALL);
// ini_set('display_errors', 'On');
// ini_set("error_log","/home/xtreamcodes/iptv_xtream_codes/wwwdir/streaming/error_log");


$dp = mysqli_connect('127.0.0.1', 'root', 'MYSQL_PASSWORD', 'xtream_iptvpro');
#

if(! $stream = $_GET['stream'] ){
	echo "cant find stream id";

} else if(! $rs = mysqli_query($dp, " SELECT `streams_sys`.`server_id`, `streaming_servers`.`server_ip` FROM `streams_sys` LEFT JOIN `streaming_servers` ON `streams_sys`.`server_id`=`streaming_servers`.`id` WHERE `stream_id`=$stream LIMIT 1 " ) ){
	echo mysqli_error($dp);

} else if(! $rw = mysqli_fetch_assoc($rs) ){
	echo "cant find the record";

} else {
	
	mysqli_close($dp);
	
	$lb_ip = $rw['server_ip'];
	$lb_port = 25461;

	$location = "http://".$lb_ip.":".$lb_port.$_SERVER['REQUEST_URI'];
	// echo $location;
	header("Location: $location");
	die();

}

mysqli_close($dp);